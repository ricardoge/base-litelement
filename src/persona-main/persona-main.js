import {LitElement, html} from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
//import '../persona-main-dm/persona-main-dm.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            maxYearsInCompany: {type: Number}
        };
    }

    constructor() {
        super();
        this.people = [];
        this.showPersonForm = false;
    }

    render() {
        /*esto es el template*/
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => person.yearsInCompany <= this.maxYearsInCompany
                    ).map(
                            person =>
                            html`<persona-ficha-listado
                                    fname="${person.name}"
                                    yearsInCompany="${person.yearsInCompany}"
                                    profile=${person.profile}
                                    .photo="${person.photo}"
                                    @delete-person="${this.deletePerson}"
                                    @info-person="${this.infoPerson}">
                            </persona-ficha-listado>`
                        )
                    }
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none"
                    @persona-form-close="${this.personaFormClose}"
                    @persona-form-store="${this.personaFormStore}"
                >
                </persona-form>
            </div>
            <persona-main-dm @updated-people-dm="${this.updatedPeopleDm}"></persona-main-dm>

        `;
    }

    personaFormStore(e)
    {
        console.log("persona-main.js ::: Se almacena una persona");

        console.log(e);

        if(e.detail.editingPerson === true) {
            //Editar
            console.log("persona-main.js ::: Actualización de persona");
            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            )

        }else {
            //Guardar
            //para que se actualice el array y lo pille "updated" es necesario
            //que cambie la referencia al objeto no solo el contenido del mismo
            this.people = [...this.people, e.detail.person];//spread syntax

        }

        this.showPersonForm = false;


    }

    updatedPeopleDm(e)
    {
        this.people = e.detail.people;

    }

    updated(changedProperties)
    {
        console.log("persona-main.js ::: Updated()");
        if(changedProperties.has("showPersonForm")) {
            console.log("persona-main.js ::: Propiedad showPersonForm updated");
            if(this.showPersonForm == true)
            {
                this.showPersonFormData();

            }else
            {
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")) {
            console.log("persona-main.js ::: Propiedad people updated");
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }

        if(changedProperties.has("maxYearsInCompany")) {
            console.log("persona-main.js ::: maxYearsInCompany updated : " + this.maxYearsInCompany);
        }

    }

    personaFormClose()
    {
        console.log("persona-main.js ::: Formulario cerrado");
        this.showPersonForm=false;
    }

    showPersonFormData()
    {
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");

    }

    showPersonList()
    {
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

	deletePerson(e) {
		console.log("persona-main.js ::: deletePerson en persona-main");
		console.log("persona-main.js ::: Se va a borrar la persona de nombre " + e.detail.name);

		this.people = this.people.filter(
			person => person.name != e.detail.name
		);
    }

	infoPerson(e) {
		console.log("persona-main.js ::: infoPerson en persona-main");
		console.log("persona-main.js ::: Se va a infoPerson la persona de nombre " + e.detail.name);

        let filterArray = this.people.filter(
			person => person.name === e.detail.name
        );
        let chosenPerson = filterArray[0];

        console.log("persona-main.js ::: " + chosenPerson);
        this.shadowRoot.getElementById("personForm").person=chosenPerson;

        console.log("persona-main.js ::: " + this.shadowRoot.getElementById("personForm").person.name);
        this.shadowRoot.getElementById("personForm").editingPerson = true;

        this.showPersonForm=true;
	}

}


/*aqui se declara el custom element*/
customElements.define('persona-main', PersonaMain)
