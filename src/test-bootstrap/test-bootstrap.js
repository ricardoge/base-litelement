import {LitElement, html, css} from 'lit-element'


class TestBootstrap extends LitElement {

    static get styles() {
        return css`
            .redbg {
                background-color:red;
            }
            .greenbg {
                background-color:green;
            }
            .bluebg {
                background-color:blue;
            }
            .greybg {
                background-color:grey;
            }                                    

        `;
    }

    render() {
        /*esto es el template*/
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <h3>Test Bootstrap</h3>
            <div class="row greybg">
                <div class="col-2 offset-1  redbg">Col 1</div>
                <div class="col-3 greenbg">Col 2</div>
                <div class="col-4 bluebg">Col 3</div>                                
            </div>

        `;
    }

}


/*aqui se declara el custom element*/
customElements.define('test-bootstrap', TestBootstrap)