import {LitElement, html} from 'lit-element'
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';


class GestorEvento extends LitElement {
    render() {
        /*esto es el template*/
        return html`
            <h1>Gestor Evento</h1>	
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e) {
        console.log("Capturado evento del emisor");
        console.log(e.detail);
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;

        
    }

}

    
/*aqui se declara el custom element*/
customElements.define('gestor-evento', GestorEvento)