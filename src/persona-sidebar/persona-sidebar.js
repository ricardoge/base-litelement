import {LitElement, html} from 'lit-element'

class PersonaSidebar extends LitElement {

    static get properties() {
        return {			
            peopleStats: {type: Object}
        };
    }	

    constructor() {
        super();
        this.peopleStats = {};
    }

    render() {
        /*esto es el template*/
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <aside>
                <section>   
                    <div>
                        <!-- data binding-->
                        Hay <span class="badge badge-pill badge-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <div class="mt-5">
                        <button class="w-100 btn btn-success" style="" @click="${this.newPerson}" ><strong>+</strong></button> 
                    </div>
                    <div>
                        <input type="range" min="0" 
                            max="${this.peopleStats.maxYearsInCompany}"
                            step="1" 
                            value="${this.peopleStats.maxYearsInCompany}"
                            @input="${this.updateMaxYearsFilter}" />
                    </div>
                
                </section>


            </aside>
        `;
    }

    updateMaxYearsFilter(e)
    {
        console.log("persona-sidebar.js ::: updateMaxYearsFilter");
        this.dispatchEvent(new CustomEvent("updated-max-years-filter", 
            {
                detail: {
                    maxYearsInCompany : e.target.value
                }

            }
        ));
    }

    newPerson(e)
    {
        console.log("persona-sidebar.js ::: newPerson");

        this.dispatchEvent(new CustomEvent("new-person", {}));

        
    }

}


/*aqui se declara el custom element*/
customElements.define('persona-sidebar', PersonaSidebar)